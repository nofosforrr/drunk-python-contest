# Задание 1. Найди всех питонов

![Pythons](../.img/baby_pythons.jpg)

## Твоя задача – найти всех спрятанных питонов в строке
Требуется написать программу, которая найдёт все слова `python`, спрятанные в переданной строке.
"Питоны" могут располагаться как целиком, например, "sdadsf**python**asdsfg", 
так и по частям – "sdfs**py**dgrr**t**gfgg**ho**dseeeeec**n**drreq234".

### Пример работы программы
```python
>>> count_pythons('sdfspydgrrtgfgghodseeeeecndrreq234')
>>> 1  
```
