# Задание 4. Пьяный Питон

![Drunk](../.img/drunk.jpeg)

## Твоя задача – научить Питона понимать, когда уже хватит 🥴
Перед тобой представлен фрагмент кода, который подскажет тебе, как напоить Питона.
Можно давать Питону пиво, маргариту и водку.

Питон весом 10 килограммов напивается, выпив 2 бутылки пива (500 мл ~4%), 
либо две маргариты (коктейль 100 мл ~20%), либо выпив два шота водки (50 мл ~40%).  
Питоны могут быть разной массы тела, которую мы сообщим на этапе проверки, 
а предлагать будем **только** перечисленные напитки в разном количестве.
Требуется дополнить метод `drink` таким образом, чтобы питон мог сказать, когда он выпил уже достаточно,
а следующий напиток сделает его пьяным.


### Шаблон
```python
from dataclasses import dataclass, field


@dataclass
class Beverage:
    """Напиток, который может выпить Питон."""
    
    name: str = field(metadata={'description': 'Вид напитка'})
    volume: int = field(metadata={'description': 'Объём напитка в мл'})
    

class DrinkingPython:
    """Класс для питонов разной массы тела."""
    
    def __init__(self, weight: int) -> None:
        self._weight = weight
        
    def drink(self, beverage: Beverage):
        ...
```

### Пример работы
```python
>>> bob_the_drinking_python = DrinkingPython(weight=18)
>>> bob_the_drinking_python.drink(Beverage(name='beer', volume=500))
>>> "Gimme more! I'm sober as a judge!"
>>> bob_the_drinking_python.drink(Beverage(name='margarita', volume=100))
>>> "Gimme more! I'm sober as a judge!"
>>> bob_the_drinking_python.drink(Beverage(name='vodka', volume=100))
>>> "Oops! I got really tipsy, can't drink anymore"
```
